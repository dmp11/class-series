import {Worker} from "./worker.js";

export class Manager extends Worker{
  constructor(first_name, second_name, salary, paidSalaries, listOfWorked){
    super(first_name, second_name, salary, paidSalaries)
    this.paidSalaries = paidSalaries
    this.listOfWorked = listOfWorked
  };
  removeOfWorked(nameOfWorked){
    let idOfWorked = this.listOfWorked.indexOf(nameOfWorked, 0);
    if (idOfWorked !== -1) {
      this.listOfWorked.splice(idOfWorked, 1);
      console.log(`${nameOfWorked} deleted`);
    }
    else {
      console.log(`${nameOfWorked} not found`);
    }
  };
}
