export class Person{
  constructor(first_name, second_name) {
    this.first_name = first_name
    this.second_name = second_name
  }
  
  showInfo() {
    console.log(`Person \n first_name: ${this.first_name}, \n second_name: ${this.second_name}\n`)
  }
}