import {Person} from "./person.js";
import {Student} from "./student.js";
import {Worker} from "./worker.js";
import {Manager} from "./manager.js";

let person = [
  new Person ("Ivan", "Ivanov"),
  new Person ("Dimon", "Dimonov"),
  new Person ("Vasil", "Vasilev"),
];

let student = [
  new Student("Petr", "Petrov", new Date(1970, 0, 22), 1),
  new Student("Petr", "Petrov", new Date(1974, 6, 1), 5),
  new Student("Petr", "Petrov", new Date(1973, 11, 13), 4)
]
let worker = new Worker(
    "Work", 
    "Workin", 
    0, 
    [
      new Date(2021, 0, 10) + " 105000", 
      new Date(2021, 1, 10) + " 110000", 
      new Date(2021, 2, 10) + " 115000"
    ]
);

let manager =  new Manager(
    "Manag", 
    "Managerov", 
    0,
    [
      new Date(2021, 0, 10) + " 195000", 
      new Date(2021, 1, 10) + " 200000", 
      new Date(2021, 2, 10) + " 205000"
    ],
    [
      "Ivan Ivanov", 
      "Petr Petrov", 
      "Work Workin"
    ]
);


worker.salary = 110000;
console.log(worker.salary);

worker.addPaid(new Date() + " 130000");
console.log(worker.paidSalaries);

manager.addPaid(new Date() + " 230000");
console.log(manager.paidSalaries);

console.log(manager.listOfWorked);
manager.removeOfWorked("Work Workin");
console.log(manager.listOfWorked);


let jsonPerson = JSON.stringify(person);
let jsonStudent = JSON.stringify(student);
let jsonWorker = JSON.stringify(worker);
let jsonManager = JSON.stringify(manager);

console.log(jsonPerson);
console.log(jsonStudent);
console.log(jsonWorker);
console.log(jsonManager);

