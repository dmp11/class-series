import {Person} from "./person.js"

export class Student extends Person {
  constructor(first_name, second_name, dateOfBirth, course) {
    super(first_name, second_name)
    this.dateOfBirth = dateOfBirth
    this.course = course
  }

  showInfo() {
    console.log(`Student \n first_name: ${this.first_name}, \n second_name: ${this.second_name}, \n date of birth: ${this.dateOfBirth}, \n course: ${this.course}\n`)
  }
}