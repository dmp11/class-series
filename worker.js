import {Person} from "./person.js";

export class Worker extends Person {
  #salary = 0;
  constructor(first_name, second_name, salary, paidSalaries) {
    super(first_name, second_name)
    this.paidSalaries = paidSalaries
  };
  set salary(salary){
    this.#salary = salary;
  };
  get salary(){
    return this.#salary;
  };
  addPaid(newPaidSalary){
    this.paidSalaries.push(newPaidSalary);
  };
}